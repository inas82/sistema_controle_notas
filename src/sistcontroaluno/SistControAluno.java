
package sistcontroaluno;

import java.util.Scanner;
import sistcontroaluno.menus.AlunosAprovados;
import sistcontroaluno.menus.AlunosReprovados;
import sistcontroaluno.menus.AtualizarAluno;
import sistcontroaluno.menus.CadrastraAluno;
import sistcontroaluno.menus.ConsultaAluno;
import sistcontroaluno.menus.ItemDeMenu;
import sistcontroaluno.menus.ListarAlunoPorNomes;
import sistcontroaluno.menus.ListarTodosAlunos;
import sistcontroaluno.menus.RemoverAluno;
import sistcontroaluno.menus.Sair;


public class SistControAluno {

    public static void main(String[] args) {
        
        Scanner teclado = new Scanner(System.in);
        
        System.out.println("\t ** BEM VINDOS AO SISTEMA DE CONTROLE DE NOTAS - KROTON ** \n");
        
        ItemDeMenu[] principal = new ItemDeMenu[]{
            
            
            new CadrastraAluno(),
            new ListarAlunoPorNomes(),
            new ConsultaAluno(),
            new AtualizarAluno(),
            new RemoverAluno(),
            new AlunosAprovados(),
            new AlunosReprovados(),
            new ListarTodosAlunos(),
            new Sair(),
            
  
        };
        System.out.println("-----------------:------------------------------:--------------------\n");
        boolean Sair = false;
        do{
           
        for (int i = 0; i < principal.length; i++){
            System.out.println(i + " - " + principal[i].getDescricao());
            
        }
         System.out.print("\n");
        System.out.print("QUAL É A OPÇAO DESEJADA: ");
        int opcao = Integer.parseInt(teclado.nextLine());
        
        Sair = principal[opcao].executar();
        
      }while(!Sair);
        
    }

    
}
