
package sistcontroaluno.inserir;

import java.util.ArrayList;

import sistcontroaluno.modelos.Alunos;



public class DaoArrayList implements Dao {

   private static ArrayList<Alunos> bd = new ArrayList<Alunos>();

     @Override
    public void Cadastra(Alunos alunos) {
       bd.add(alunos);
    }

    @Override
    public ArrayList<Alunos> Listar() {
       return bd;
    }

    @Override
    public Alunos pesquisar(String matricula) {
         Alunos resultado = null;
        
         for(int i = 0; i < bd.size(); i++){
             
             Alunos atual = bd.get(i);
             
             if(atual.getMatricula().equals(matricula)){
                 resultado = atual;
                 break;
             }
             
         }
         
       return resultado;
    }

    @Override
    public void atualizar(Alunos alunos) {
       Alunos pesquisado = pesquisar(alunos.getMatricula());
       
       pesquisado.setNome(alunos.getNome());
       pesquisado.setP1(alunos.getP1());
       pesquisado.setP2(alunos.getP2());
       pesquisado.setOficial1(alunos.getOficial1());
       pesquisado.setOficial2(alunos.getOficial2());
    }

    @Override
    public void remover(Alunos alunos) {
        bd.remove(alunos);
    }

  
  
    
}
