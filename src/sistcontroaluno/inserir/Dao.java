
package sistcontroaluno.inserir;

import java.util.ArrayList;
import sistcontroaluno.modelos.Alunos;


public interface Dao {
    
    public abstract void  Cadastra(Alunos alunos);
        
        
     ArrayList<Alunos> Listar();

     Alunos pesquisar (String matricula);
     
     void atualizar(Alunos alunos);
       
      void remover(Alunos alunos);
    
}
