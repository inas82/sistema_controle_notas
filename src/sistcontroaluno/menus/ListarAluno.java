
package sistcontroaluno.menus;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import sistcontroaluno.modelos.Alunos;

public abstract class ListarAluno extends ItemDeMenu implements Comparator<Alunos>{
    
    @Override
    public boolean executar() {
        
       
        ArrayList<Alunos> alunos = dao.Listar();
        
        Collections.sort(alunos, this);
        
        for(int i =0; i < alunos.size(); i++){
            
            Alunos atual = alunos.get(i);
            
            if(deverImprimir(atual)){
            System.out.print("\n");
            System.out.println("NOME DO ALUNO: " + atual.getNome()+ " - " + "NUMERO DA MATRICULA: "+atual.getMatricula()+"\n");
            System.out.println("NOTA DAS PARCIAIS: " + atual.aprovadop()+ " - " + "NOTA DAS OFICIAIS:  "+atual.passouof()+" "+atual.getStatus()+" NAS OFICIAIS\n");
            System.out.println("MÉDIA TOTAL: " + atual.MediaTotal()+" "+atual.getStatus() + " NA DISCIPLINA\n"); 
            }
            
        } 
        
         return false;
    }

    public  abstract boolean deverImprimir(Alunos alunos);
    
    @Override
    public int compare(Alunos o1, Alunos o2) {
        String nome1 = o1.getNome();
        String nome2 = o2.getNome();
        
        return nome1.compareTo(nome2);
    }
}

