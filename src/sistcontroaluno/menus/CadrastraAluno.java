
package sistcontroaluno.menus;
import sistcontroaluno.modelos.Alunos;

 public class CadrastraAluno extends ItemDeMenu {

    public CadrastraAluno() {
    }

    @Override
    public String getDescricao() {
  
        return "CADRASTRA ALUNO";
    }

    @Override
    public boolean executar() {
       
       System.out.println("-----------------:------------------------------:--------------------\n");
        String nome = leitor.lerString("NOME DO ALUNO: ");
        
        String matricula = leitor.lerString("NUMERO DE MATRÍCULA: ");
        
        double p1 = leitor.lerDouble("NOTA DA PARCIAL 1 (MAX: 1000): ");
        
        double p2 = leitor.lerDouble("NOTA DA PARCIAL 2 (MAX: 1500): ");
        
        double oficial1 = leitor.lerDouble("NOTA DA OFICIAL 1 (MAX: 1000): ");
        
        double oficial2 = leitor.lerDouble("NOTA DA OFICIAL 2(MAX: 4000): ");
        
        System.out.println("\nA NOTA DE APROVAÇÃO DAS OFICIAIS (MINIMO: 2500), A APROVAÇÃO DA DISCPLINA (MINIMO: 70000)");
        
        Alunos alunos = new Alunos(matricula, nome);
        
        alunos.setP1(p1);
        alunos.setP2(p2);
        alunos.setOficial1(oficial1);
        alunos.setOficial2(oficial2);
        
        dao.Cadastra(alunos);
        System.out.print("\n");
         System.out.println("-----------------:------------------------------:--------------------\n");
        System.out.println("O ALUNO FOI CADASTRADO COM SUCESSO!!");
         System.out.println("-----------------:------------------------------:--------------------\n");
        System.out.println("\n");
        
         return false;
    }
    
}
