
package sistcontroaluno.menus;

import sistcontroaluno.modelos.Alunos;


public class ConsultaAluno extends ItemDeMenu {

    @Override
    public String getDescricao() {
       return "CONSULTA ALUNO";
    }

    @Override
    public boolean executar() {
         System.out.println("-----------------:------------------------------:--------------------\n");
        
          String matricula = leitor.lerString("NUMERO DE MATRÍCULA: ");
          System.out.println("\n");
         
         Alunos alunos = dao.pesquisar(matricula);
        
         if(alunos == null){
              System.out.println("-----------------:------------------------------:--------------------\n");
             System.out.println("ALUNO NÃO ENCONTRADO \n");
              System.out.println("-----------------:------------------------------:--------------------\n");
         }else{
             System.out.println("-----------------:------------------------------:--------------------\n");
             System.out.print("NOME : "+ alunos.getNome()+"\n");
             System.out.print("PARCIAL 1 (MAX: 1000) : "+ alunos.getP1()+"\n");
             System.out.print("PARCIAL 2 (MAX: 1500): "+ alunos.getP2()+"\n");
             System.out.print("NOTAS DAS PARCIAIS(MAX DE NOTA NA PARCIAL: 2500) : "+ alunos.aprovadop()+"\n");
             System.out.print("OFICIAL 1 (MAX: 1000): "+ alunos.getOficial1()+"\n");
             System.out.print("OFICIAL 2 (MAX: 4000): "+ alunos.getOficial2()+"\n");
             System.out.print("NOTA DAS OFICIAIS (MINIMO PARA APROVAÇAO DAS OFICIAL: 2500) : "+ alunos.passouof()+"\n");
              System.out.print("-----------------:------------------------------:--------------------\n");
              System.out.println("O ALUNO FOI CONSULTADO COM SUCESSO!!");
               System.out.println("-----------------:------------------------------:--------------------\n");
        System.out.println("\n");
        
         }
                 
         return false;
    }
    
}
