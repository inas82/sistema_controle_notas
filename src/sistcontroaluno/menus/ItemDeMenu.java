
package sistcontroaluno.menus;

import sistcontroaluno.inserir.Dao;
import sistcontroaluno.inserir.DaoArrayList;
import sistcontroaluno.io.Leitor;


public  abstract class ItemDeMenu {
      
    protected Leitor leitor;
    protected Dao dao;
    
    public  ItemDeMenu(){
        leitor = new Leitor();
        dao = new DaoArrayList();
        
    }
    
    public abstract String getDescricao();
    public abstract boolean executar();
    
}


