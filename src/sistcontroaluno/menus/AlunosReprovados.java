
package sistcontroaluno.menus;

import sistcontroaluno.modelos.Alunos;


public class AlunosReprovados extends ListarAluno {
    
@Override
    public String getDescricao() {
     return "LISTAR ALUNOS REPROVADOS";   
    }
   
    public boolean deverImprimir(Alunos alunos) {
        return !alunos.aprovadoTotal();
    }
    

}
