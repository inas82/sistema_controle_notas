
package sistcontroaluno.menus;

import sistcontroaluno.modelos.Alunos;


public class AlunosAprovados extends ListarAluno {
    
@Override
    public String getDescricao() {
        return "LISTAR ALUNOS APROVADOS";
    }
   
    public boolean deverImprimir(Alunos alunos) {
       
        return alunos.aprovadoTotal();
    }



   
}
