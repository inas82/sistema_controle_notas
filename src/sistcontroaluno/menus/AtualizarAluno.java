
package sistcontroaluno.menus;

import sistcontroaluno.modelos.Alunos;


public class AtualizarAluno extends ItemDeMenu{

    @Override
    public String getDescricao() {
      return "ATUALIZAR DADOS DO ALUNO";
    }

    @Override
    public boolean executar() {
        
          String matricula = leitor.lerString("NUMERO DE MATRÍCULA: ");
          System.out.println("\n");
          
            Alunos alunos = dao.pesquisar(matricula);
        
            if(alunos == null){
             
             System.out.println("ALUNO NÃO ENCONTRADO \n");
                System.out.println("-----------------:------------------------------:--------------------\n");
         }else{
                
                 System.out.println("NOME DO ALUNO: " + alunos.getNome());
                 System.out.println("PARCIAL 1 (MAX: 1000) : "+ alunos.getP1()+"\n");
             System.out.println("PARCIAL 2 (MAX: 1500): "+ alunos.getP2()+"\n");
             System.out.println("NOTAS DAS PARCIAIS(MAX DE NOTA NA PARCIAL: 2500) : "+ alunos.aprovadop()+"\n");
             System.out.println("OFICIAL 1 (MAX: 1000): "+ alunos.getOficial1()+"\n");
             System.out.println("OFICIAL 2 (MAX: 4000): "+ alunos.getOficial2()+"\n");
             System.out.println("NOTA DAS OFICIAIS (MINIMO PARA APROVAÇAO DAS OFICIAL: 2500) : "+ alunos.passouof()+"\n");
              System.out.println("-----------------:------------------------------:--------------------\n");
             String nome = leitor.lerString("ADICIONAR NOVO NOME: ");
                System.out.print("\n");
             double p1 = leitor.lerDouble("ADICIONAR NOVA NOTA DA PARCIAL 1 (MAX: 1000): ");
             System.out.print("\n");
             double p2 = leitor.lerDouble("ADICIONAR NOVA NOTA DA PARCIAL 2 (MAX: 1500): ");
             System.out.print("\n");
             double oficial1 = leitor.lerDouble("ADICIONAR NOVA NOTA DA OFICIAL 1 (MAX: 1000): ");
             System.out.print("\n");
             double oficial2 = leitor.lerDouble("ADICIONAR NOVA NOTA DA OFICIAL 2 (MAX: 4000): ");
             System.out.print("\n");
             
             alunos.setNome(nome);
             alunos.setP1(p1);
             alunos.setP2(p2);
             alunos.setOficial1(oficial1);
             alunos.setOficial2(oficial2);
             
             dao.atualizar(alunos);
              System.out.println("-----------------:------------------------------:--------------------\n");
             System.out.println("O  DADOS DO ALUNO FOI ATUALIZADO COM SUCESSO!!");
              System.out.println("-----------------:------------------------------:--------------------");
             System.out.println("\n");
            }
            
         return false;
  
    }
      
}
