
package sistcontroaluno.menus;

import sistcontroaluno.modelos.Alunos;

public class RemoverAluno extends ItemDeMenu {

    @Override
    public String getDescricao() {
      return "REMOVER ALUNO"; 
    }

    @Override
    public boolean executar() {
        
          String matricula = leitor.lerString("NUMERO DE MATRÍCULA: ");
           System.out.println("\n");
           
            Alunos alunos = dao.pesquisar(matricula);
            
            if(alunos == null){
                 System.out.println("-----------------:------------------------------:--------------------\n");
                System.out.println("ALUNO NÃO ENCONTRADO \n"); 
                 System.out.println("-----------------:------------------------------:--------------------\n");
            }else{
                dao.remover(alunos);
                 System.out.println("-----------------:------------------------------:--------------------\n");
                System.out.println("ALUNO FOI REMOVIDO COM SUCESSO!! \n");
                 System.out.println("-----------------:------------------------------:--------------------\n");
            }
        
        return false;
    }
    
    }