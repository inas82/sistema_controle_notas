/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistcontroaluno.menus;

import sistcontroaluno.modelos.Alunos;

/**
 *
 * @author JESSICA
 */
public class ListarTodosAlunos extends ListarAluno {
    

    public String getDescricao() {
        return "LISTAR TODOS OS ALUNOS";
    }
    @Override
    public boolean deverImprimir(Alunos alunos) {
        return true;
    }

    @Override
    public int compare(Alunos o1, Alunos o2){
       double Media1 = o1.MediaTotal();
       double Media2 = o2.MediaTotal();
       
       if(Media1 < Media2){
           return -1;
       }
       else if(Media1 > Media2){
           
           return 1;
       }else{
         
        return super.compare(o1, o2);
           
       }
         
    }
    
    
}
