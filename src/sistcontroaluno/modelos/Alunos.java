
package sistcontroaluno.modelos;

//
public class Alunos {
    
    private String matricula, nome ,Status ;
    private double p1, p2, oficial1, oficial2, Mediap,Mediaof, MediaTotal;
    

    public Alunos(String matricula, String nome) {
        this(matricula, nome, 0, 0, 0, 0);
        
    }

    public Alunos(String matricula, String nome, double p1, double p2, double oficial1, double oficial2) {
        this.matricula = matricula;
        this.nome = nome;
        this.p1 = p1;
        this.p2 = p2;
        this.oficial1 = oficial1;
        this.oficial2 = oficial2;
       
        
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String Status) {
        this.Status = Status;
    }

    public double getP1() {
        return p1;
    }

    public void setP1(double p1) {
        this.p1 = p1;
    }

    public double getP2() {
        return p2;
    }

    public void setP2(double p2) {
        this.p2 = p2;
    }

    public double getMediap() {
        return Mediap;
    }

    public void setMediap(double Mediap) {
        this.Mediap = Mediap;
    }

    public double getMediaof() {
        return Mediaof;
    }

    public void setMediaof(double Mediaof) {
        this.Mediaof = Mediaof;
    }

    
// CALCULAR PARCIAL
    public double aprovadop(){
        Mediap = (p1 + p2);
       
        if(Mediap == 2500){
            
            Status ="APROVADO";
            
        }
           
              return Mediap;
        }
    
    
    public double getOficial1() {
        
        return oficial1;
    }

    public void setOficial1(double oficial1) {
       
        this.oficial1 = oficial1;
    }

    public double getOficial2() {
    
        return oficial2;
    }

    public void setOficial2(double Oficial2) {
        this.oficial2 = Oficial2;
    }
    // CALCULAR OFICIAL
    public double passouof(){
        
        Mediaof = (oficial1 + oficial2);
        
        if(Mediaof >= 2500){
            
              Status ="APROVADO";
           
        }else{
             Status ="REPROVADO";
          
            
        }
        return Mediaof;
    }    

    public double getMediaTotal() {
        
      
        return MediaTotal;
    }

    public void setMediaTotal(double MediaTotal) {
        this.MediaTotal = MediaTotal;
    }
    
// Calcular Total
    
    public double MediaTotal(){
        MediaTotal =(Mediap + Mediaof);
        
        if(MediaTotal < 7000){
            
            Status = "REPROVADO";
        } else{
            
            Status = "APROVADO";
            
        }
        
       return MediaTotal;
    }
    
 public boolean aprovadoTotal(){
     
     return MediaTotal >=7000 && MediaTotal <=12000;
 }
 
}
